jQuery(function ($) {
	$('a.smooth-scroll[href^=#]').click(function (e) {
		e.preventDefault();
		var headerHeight = 80;
		var win = $(window).width();
		if (win < 769) {
			headerHeight = 70;
		}
		var speed = 400;
		var href = jQuery(this).attr("href");
		var target = jQuery(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top - headerHeight;
		$('body,html').animate({scrollTop: position}, speed, 'swing');
		return false;
	});
});

jQuery(function ($) {
	$('html').imagesLoaded(function () {
		$('.main-slideshow').slick({
			dots: true,
			arrows: false,
			autoplay: true,
			autoplaySpeed: 5000,
			speed: 500,
			fade: true,
			cssEase: 'linear',
			pauseOnHover: false
		});
		$('.img-link-boxes').slick({
			slidesToShow: 3,
			responsive: [
				{
					breakpoint: 767,
					settings: {
						dots: false,
						arrows: true,
						centerMode: true,
						slidesToShow: 1
					}
				}
			]
		});
	});
});

jQuery(function ($) {
	$('#menu-sp-btn').on('click', function (e) {
		e.preventDefault();
		$(this).toggleClass('is-clicked');
		$('.nav-container').toggleClass('is-display');
	})
});

jQuery(function ($) {
	$(window).on('load scroll resize', function () {
		var scrollAmount = $('.main-banner').outerHeight();
		if ($(window).scrollTop() > scrollAmount) {
			$('#main-header').addClass('solid');
		}
		else {
			$('#main-header').removeClass('solid');
		}
	})
});

jQuery(document).ready(function ($) {
	$(".tab-list a").click(function (e) {
		e.preventDefault();
		$('.tab-list a').removeClass('active');
		$(this).addClass('active');
		var thisFilter = $(this).attr('data-filter');
		if (thisFilter === 'all') {
			$('.news-list .news-list-item').show();
		}
		else {
			$('.news-list .news-list-item').not('.' + thisFilter).hide();
			$('.news-list .news-list-item.' + thisFilter).show();
		}
	});
});

// Don't write any code below this line
jQuery(document).ready(function ($) {
	var offsetSize = $("#main-header").innerHeight();
	$("html, body").animate({scrollTop: $(window.location.hash).offset().top - offsetSize - 50}, 500);
});